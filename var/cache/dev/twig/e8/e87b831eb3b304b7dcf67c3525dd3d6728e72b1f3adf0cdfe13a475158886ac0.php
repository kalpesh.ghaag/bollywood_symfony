<?php

/* form.html.twig */
class __TwigTemplate_cd7f300a6dee1ed519b2f004891a8a1f4fea685ef45e8401cf8d945791221443 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>

<body>
    <a href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("first_form");
        echo "\">Movies</a>
    <form action=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("first_form");
        echo "\">
        <label for=\"id\">ID</label>
        <input type=\"number\" name=\"id\" id=\"id\" placeholder=\"Movie ID\">
        <label for=\"name\">Movie Name</label>
        <input type=\"text\" name=\"name\" id=\"name\" placeholder=\"Name\">
        <label for=\"date\">Release Date</label>
        <input type=\"date\" name=\"date\" id=\"date\" placeholder=\"Date\">
        <label for=\"director\">Director</label>
        <input type=\"text\" name=\"director\" id=\"director\" placeholder=\"Director\">

        <button>Submit</button>
    </form>

</body>

</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 13,  42 => 12,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>

<body>
    <a href=\"{{ path(\"first_form\") }}\">Movies</a>
    <form action=\"{{ path(\"first_form\") }}\">
        <label for=\"id\">ID</label>
        <input type=\"number\" name=\"id\" id=\"id\" placeholder=\"Movie ID\">
        <label for=\"name\">Movie Name</label>
        <input type=\"text\" name=\"name\" id=\"name\" placeholder=\"Name\">
        <label for=\"date\">Release Date</label>
        <input type=\"date\" name=\"date\" id=\"date\" placeholder=\"Date\">
        <label for=\"director\">Director</label>
        <input type=\"text\" name=\"director\" id=\"director\" placeholder=\"Director\">

        <button>Submit</button>
    </form>

</body>

</html>", "form.html.twig", "/var/www/html/bollywood/templates/form.html.twig");
    }
}
