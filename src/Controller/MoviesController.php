<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Movies;
use App\Form\MoviesType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class MoviesController extends AbstractController
{

    /**
     * @Route("/form", name="first_form")
     */
    public function first()
    {
        return $this->render("form.html.twig", []);
    }
    /**
     * @Route("/movieForm", name="form_simple")
     */
    public function movieForm(Request $request)
    {
        $movies= new Movies();
        $form = $this->createForm(MoviesType::class, $movies);
        $form->add('submit', SubmitType::class, [
            'label' => 'Create',
            'attr' => ['class' => 'btn btn-default pull-right'],
        ]);
        $form->handleRequest($request);
        dump($form);
        $moviesData = $form->getData();
        return $this->render("form.html.twig", [
            'form'=> $form->createView(),
            'moviesData'=> $moviesData
            ]);
    }
    
}

// var_dump(<p>'bloup'</P>);
